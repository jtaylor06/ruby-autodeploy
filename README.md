# Sample Ruby application for Auto Deploy feature

Minimal Ruby project configured to use GitLab Auto Deploy feature with Kubernetes

## How to use

1. Clone the repository
2. Go to Kubernetes integration and add your cluster info there
3. Modify `.gitlab-ci.yml` with your domain name
4. Ensure pipeline with deploy job succeed
4. Install nginx-ingress via helm: `helm install stable/nginx-ingress`
5. Point your domain name to nginx-ingress-controller external IP address

TODO: Add link to the full guide that uses this repo
